# Enunciado 3

> Implementar un método de verificación lógica de paréntesis. Es decir, implementar el método `parenthesisChecker(str)` que recibe un `string` como parámetro y devuelve un `boolean`. La respuesta del método debe ser `true` si la cadena de `string` es válida en términos de paréntesis (`( )`, `[ ]`, `{ }`), i.e. cada apertura de paréntesis se cierra correctamente. A continuación se muestran ejemplos de `string` válidos e inválidos.
> 
> **Ejemplos válidos**: la función debe devuelve `true`.
>
> - `parenthesisChecker('a * (b + c)')` → `true`
> - `parenthesisChecker('a * (b + c * [d])')` → `true`
> - `parenthesisChecker('[]{}()abc{([])}')` → `true`
>
> **Ejemplos válidos**: la función debe devuelve `false`.
>
> - `parenthesisChecker('(()')` → `false`
> - `parenthesisChecker('(([))')` → `false`
> - `parenthesisChecker('([)]')` → `false`

# Razonamiento

> Investigue sobre las expresiones regulares e implemente las expresiones regulares para verificar que la cadena fuera correcta.
> Verifique parentesis.test(str) === parentesi.test(str) para saber si en la cadena existia o no un parentesis en caso de que no existiera no debia de fallar porque debia continuar buscando con las otras expresiones regulares, por eso cuando fallaban ambas expresiones aun asi daba verdadero permitiendo seguir con las consultas de las demas expresiones.