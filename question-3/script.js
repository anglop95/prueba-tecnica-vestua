/**
 * On this module you should write your answer to question #3.
 * This file would be executed with the following command:
 * $ node scritp.js 'a * (b + c)'
 */

const args = process.argv.slice(-1);
console.log(`Running question #3 with args ${args}`)

/**
 * Check if a string has correct use of parenthesis.
 * 
 * @param {String} str - String to be evaluated
 * @returns {Boolean} Returns true if string is valid.
 */
function parenthesisChecker(str) {
    parentesis = /\((?:[^)(]+|\((?:[^)(]+|\([^)(]*\))*\))*\)/gm;
    parentesi = /\(|\)+/gm;
    corchetes = /\[(?:[^\]\[]+|\[(?:[^\]\[]+|\[[^\]\[]*\])*\])*\]/gm;
    corchete = /\[|\]+/gm;
    llaves = /\{(?:[^}{]+|\{(?:[^}{]+|\{[^}{]*\})*\})*\}/gm;
    llave = /\{|\}+/gm;

    console.log(parentesis.test(str) === parentesi.test(str));
    console.log(corchetes.test(str) === corchete.test(str));
    console.log(llaves.test(str) === llave.test(str));
    if ((parentesis.test(str) === parentesi.test(str)) && (corchetes.test(str) === corchete.test(str)) && (llaves.test(str) === llave.test(str))) {
        return true;
    }
    return false;
}

cadena = '[]{}()abc{([])}';
const isValid = parenthesisChecker(cadena);
console.log(`parenthesisChecker("${cadena}") = ${isValid}`);

