# Enunciado 2

> En la carpeta [question-2](https://bitbucket.org/vestua-com/questions/src/main/question-2/) se ha exportado eventos de navegación de usuarios anonimizados de la plataforma Vestuá. Se le pide al equipo de Ingeniería que hagan un análisis sobre los datos de navegación. En particular se solicita:
>
> - Calcular la cantidad de visitas únicas por cada producto.
> - Calcular la cantidad de clicks únicos por cada producto.
> - Calular el CTR (*Clickthrough Rate*) de cada producto.
> 
> El set de datos contiene la siguiente estructura:
> 
> - `user`: id del usuario que ejecutó el evento.
> - `entityId`: id de la entidad al que el usuario ejecutó el evento.
> - `entityType`: tipo de entidad al que se ejecutó el evento.
> - `eventType`: tipo de evento. Puede ser `impression` o `click`.
> 
> Como miembro del equipo de ingeniería, te solicitan modificar el archivo [script.js](https://bitbucket.org/vestua-com/questions/src/main/question-2/script.js) para que pueda leer el set de datos y generar un archivo `output.csv` con las siguientes columnas:
> 
> - `productId`: id del producto.
> - `clicks`: cantidad de *clicks* únicos que tiene el producto
> - `impressions`: cantidad de impresiones únicas que tiene el producto.
> - `ctr`: métrica CTR del producto.

# Razonamiento

> Dado que era un archivo csv implemente fs para leer y escribir archivos

> Luego mientras leia el archivo lo agregaba un arreglo con cada linea del csv convirtiendo la cadena en arreglos para poder procesarlo luego de manera mas facil.

> Una vez almacené el csv comencé a recorrer el arreglo resultante realizando un filtrado donde obtenia una matriz con todas las coincidencia de productId, de modo que podia volver a filtrar la matriz obtenida para buscar los productos cuyos eventos eran impression o click para asi ir generando un nuevo arreglo que seria el nuevo csv resultante. Para guardar los datos en el nuevo arreglo verificaba que la linea de datos a ingresar no estuviera ya guardada y asi evitar que los datos se repitieran

> Por ultimo una vez terminaba todo el proceso de generar el nuevo arrelgo lo recorri de nuevo con la finalidad de almacenar todo en forma de string para poder generar el archivo csv nuevo con los datos requeridos