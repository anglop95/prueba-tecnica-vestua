/**
 * On this module you should write your answer to question #2.
 * This file would be executed with the following command:
 * $ node scritp.js BrowsingEvents.csv
 */

const args = process.argv.slice(-1);
console.log(`Running question #2 with args ${args}`)

const fs = require("fs");
var data = fs.readFileSync("question-2\\BrowsingEvents.csv", "utf8");
data = data.split("\r\n");
for (let i in data) {
  data[i] = data[i].split(",");
}

var newData = ['productId,clicks,impressions,ctr'];
for (let index = 1; index < data.length; index++) {
    let elementNew = data.filter(function(item) { return item[1] === data[index][1]; })
    const postId = data[index][1];
    const impressions = elementNew.filter(function(item) { return item[3] === 'impression'; }).length;
    const clicks = elementNew.filter(function(item) { return item[3] === 'click'; }).length;
    const ctr = parseFloat(impressions === 0 || clicks === 0 ? 0 : clicks/impressions).toFixed(4);
    if (newData.indexOf(postId + ',' + clicks + ',' + impressions + ',' + ctr) < 0) {
        newData.push( postId + ',' + clicks + ',' + impressions + ',' + ctr);
    }
}

var csvContent = '\ufeff' + newData[0] + '\n'
for (let index = 1; index < newData.length; index++) {
    csvContent += newData[index] + '\n'
}
fs.writeFileSync("question-2\\output.csv", csvContent);
console.log('Fin de la ejecución.');