/**
 * On this module you should write your answer to question #1.
 * This file would be executed with the following command (N=100):
 * $ node scritp.js 100
 */

function FormasDeSubir(peldanos) {
    if (peldanos < 2) {
        return peldanos;
    } else {
        return FormasDeSubir(peldanos-1) + FormasDeSubir(peldanos-2)
    }
}

const args = process.argv.slice(-1);
console.log(`Running question #1 with args ${args}`)

let nPeldano = 4;
console.log('El numero de peldaños es: ' + nPeldano);
console.log('El numero de peldaños es: ' + FormasDeSubir(nPeldano+1));