# Enunciado 1

> Estás subiendo una escalera de N peldaños. En cada momento, puedes subir 1 o 2 peldaños. ¿En cuántas formas diferentes puedes subir las escalera?

# Razonamiento

> Realice el ejercicio con 1, 2, 3, 4 y 5 y observe que seguia el patrón de la serie de fibonacci, pero, observe que si calculo la serie de fibonnacci con el mismo numero de peldaños de las escaleras no obtenia el valor correcto de los ejercicios realizados del 1 - 5 mensionados anteriormente, por lo que para obtener el numero correcto de las diferentes formas de subir las escaleras le sume uno al numero de peldaños a subir, para luego calcular la serie de fibonnacci del numero de peldaños + 1.